/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

enum Type {
   EMPLOYEE, MANAGER;
}

public class EmployeeFactory {
   private static EmployeeFactory employeeFactory = null;

   private EmployeeFactory() {

   }

   public static EmployeeFactory getInstance() {
       if (employeeFactory == null) {
           employeeFactory = new EmployeeFactory();
       }
       return employeeFactory;
   }

   public Employee getEmployee(Type type) {
       if (type == Type.MANAGER) {
           return new Manager("Jackey", 5000, 5);
       } else if (type == Type.EMPLOYEE) {
           return new Employee("Janu", 560, 2);
       }
       return null;
   }
}