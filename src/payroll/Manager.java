/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

public class Manager extends Employee {

    public double bonus;

    public Manager(String name, double hourlyWage, int hours) {
        super(name, hourlyWage, hours);
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public double calculatePay()
    {
        double pay= this.hours * this.hourlyWage + this.bonus;
        return pay;
    }

}
